﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using WebApplication.Models.ViewModels;
namespace WebApplication.Controllers
{
    public class ZamestnanecsController : Controller
    {
        private WebAppDataModel db = new WebAppDataModel();

        // GET: Zamestnanecs
        public ActionResult Index()
        {
            //var zamestnanec = db.Zamestnanec.Include(z => z.Oddeleni);
            //return View(zamestnanec.ToList());

            var items = from i in db.Zamestnanec
                        select new VMZamestnanecsIndex()
                        {
                            Id = i.Id,
                            Jmeno = i.Jmeno,
                            Prijmeni = i.Prijmeni,
                            NazevOddeleni = i.Oddeleni.Nazev,

                        };

            return View(items.ToList());

        }

        public ActionResult Index2()
        {
            return View();
        }

        // GET: Zamestnanecs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            if (zamestnanec == null)
            {
                return HttpNotFound();
            }
            return View(zamestnanec);
        }

        // GET: Zamestnanecs/Create
        public ActionResult Create()
        {
            ViewBag.idOddeleni = new SelectList(db.Oddeleni, "Id", "Nazev");
            return View();
        }

        // POST: Zamestnanecs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Jmeno,Prijmeni,idOddeleni")] Zamestnanec zamestnanec)
        {
            if (ModelState.IsValid)
            {
                db.Zamestnanec.Add(zamestnanec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idOddeleni = new SelectList(db.Oddeleni, "Id", "Nazev", zamestnanec.idOddeleni);
            return View(zamestnanec);
        }

        // GET: Zamestnanecs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            if (zamestnanec == null)
            {
                return HttpNotFound();
            }
            ViewBag.idOddeleni = new SelectList(db.Oddeleni, "Id", "Nazev", zamestnanec.idOddeleni);
            return View(zamestnanec);
        }

        // POST: Zamestnanecs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Jmeno,Prijmeni,idOddeleni")] Zamestnanec zamestnanec)
        {
            if (ModelState.IsValid)
            {
                db.Entry(zamestnanec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idOddeleni = new SelectList(db.Oddeleni, "Id", "Nazev", zamestnanec.idOddeleni);
            return View(zamestnanec);
        }

        // GET: Zamestnanecs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            if (zamestnanec == null)
            {
                return HttpNotFound();
            }
            return View(zamestnanec);
        }

        // POST: Zamestnanecs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            db.Zamestnanec.Remove(zamestnanec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
