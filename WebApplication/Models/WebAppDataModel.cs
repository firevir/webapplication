namespace WebApplication.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WebAppDataModel : DbContext
    {
        public WebAppDataModel()
            : base("name=DefaultConnection")
        {
        }

        public virtual DbSet<Oddeleni> Oddeleni { get; set; }
        public virtual DbSet<Zamestnanec> Zamestnanec { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Oddeleni>()
                .HasMany(e => e.Zamestnanec)
                .WithOptional(e => e.Oddeleni)
                .HasForeignKey(e => e.idOddeleni);
        }
    }
}
