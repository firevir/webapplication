﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.ViewModels
{
    public class VMZamestnanecsIndex
    {

        public int Id { get; set; }

        public string Jmeno { get; set; }

        public string Prijmeni { get; set; }

        [Display(Name = "VMZamestnanecsIndex_NazevOddeleni",
    ResourceType = typeof(Properties.Localization))]
        public string NazevOddeleni { get; set; }

    }
}